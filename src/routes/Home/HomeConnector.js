import { connect } from 'react-redux'
import compose from 'lodash/flowRight'
import { homeLoading, homeError, homeList } from 'src/store/main/selectors'
import { HomeGetAction } from 'src/store/actions/HomeAction'

const mapStateToProps = state => ({
  isLoading: homeLoading(state),
  error: homeError(state),
  list: homeList(state)
})

const mapDispatchToProps = {
  HomeGetAction
}

export default compose(connect(mapStateToProps, mapDispatchToProps))
