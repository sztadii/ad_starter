import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Table, Button, Alert, Loading } from 'element-react'
import columns from './columns'
import connector from './HomeConnector'

export class Home extends Component {
  componentDidMount () {
    this.props.HomeGetAction()
  }

  refresh = () => {
    this.props.HomeGetAction(true)
  }
  renderList = list => (
    <Table style={{ width: '100%' }} columns={columns} data={list} />
  )

  render () {
    const { isLoading, error, list } = this.props

    return (
      <div>
        {isLoading ? <Loading /> : null}

        {error ? <Alert title={error} type='error' /> : null}

        {list.length ? this.renderList(list) : null}

        <Button onClick={this.refresh} type='primary' className='mt-1'>
          Refresh
        </Button>
      </div>
    )
  }
}

Home.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.string.isRequired,
  list: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      email: PropTypes.string.isRequired,
      username: PropTypes.string.isRequired,
      website: PropTypes.string.isRequired,
      phone: PropTypes.string.isRequired
    })
  ).isRequired,
  HomeGetAction: PropTypes.func.isRequired
}

export default connector(Home)
