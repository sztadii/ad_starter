import React from 'react'
import { cleanup, wait } from 'react-testing-library'
import renderWithStore from '../../utils/renderWithStore'
import { nockUsers } from '../../api/api.nock'
import Home from './Home'

afterEach(cleanup)

test('Home displays information fetched from backend', async () => {
  nockUsers()

  const { getByText } = renderWithStore(<Home />)
  await wait(() => {
    getByText('Leanne Graham')
  })
})
