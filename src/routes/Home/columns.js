export default [
  {
    label: 'Id',
    prop: 'id',
    width: 60
  },
  {
    label: 'Name',
    prop: 'name'
  },
  {
    label: 'Username',
    prop: 'username'
  },
  {
    label: 'Email',
    prop: 'email'
  },
  {
    label: 'Phone',
    prop: 'phone'
  }
]
