import { connect } from 'react-redux'
import compose from 'lodash/flowRight'
import { infoLoading, infoError, infoList } from 'src/store/main/selectors'
import { InfoGetAction } from 'src/store/actions/InfoAction'

const mapStateToProps = state => ({
  isLoading: infoLoading(state),
  error: infoError(state),
  list: infoList(state)
})

const mapDispatchToProps = {
  InfoGetAction
}

export default compose(connect(mapStateToProps, mapDispatchToProps))
