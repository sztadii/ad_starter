import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Table, Alert, Loading } from 'element-react'
import columns from './columns'
import connector from './InfoConnector'

export class Info extends Component {
  componentDidMount () {
    this.props.InfoGetAction()
  }

  renderList = list => (
    <Table style={{ width: '100%' }} columns={columns} data={list} />
  )

  render () {
    const { isLoading, error, list } = this.props

    return (
      <div>
        {isLoading ? <Loading /> : null}

        {error ? <Alert title={error} type='error' /> : null}

        {list.length ? this.renderList(list) : null}
      </div>
    )
  }
}

Info.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.string.isRequired,
  list: PropTypes.array.isRequired,
  InfoGetAction: PropTypes.func.isRequired
}

export default connector(Info)
