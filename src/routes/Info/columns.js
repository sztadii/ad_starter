export default [
  {
    label: 'Id',
    prop: 'id',
    width: 50
  },
  {
    label: 'Name',
    prop: 'name'
  },
  {
    label: 'Email',
    prop: 'email'
  },
  {
    label: 'Phone',
    prop: 'phone'
  }
]
