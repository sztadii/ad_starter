import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import './styles/index.scss'
import store from './store/main/initStore'
import AuthRoute from './utils/AuthRoute'
import App from './App'

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <Switch>
        <AuthRoute path='/' component={App} />
      </Switch>
    </Provider>
  </BrowserRouter>,
  document.querySelector('#root')
)
