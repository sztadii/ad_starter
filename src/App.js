import React from 'react'
import PropTypes from 'prop-types'
import { Route, Switch } from 'react-router-dom'
import loadable from 'loadable-components'
import routes from './config/routes'

const Home = loadable(() => import('./routes/Home/Home'))
const Info = loadable(() => import('./routes/Info/Info'))

const App = ({ location }) => (
  <div className='container mt-1'>
    <Route
      location={location}
      key={location.key}
      component={({ match }) => (
        <Switch>
          <Route exact path={`${match.path}${routes.home}`} component={Home} />
          <Route exact path={`${match.path}${routes.info}`} component={Info} />
        </Switch>
      )}
    />
  </div>
)

App.propTypes = {
  location: PropTypes.object.isRequired
}

export default App
