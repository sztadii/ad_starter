const Contract = require('consumer-contracts').Contract
const Joi = require('consumer-contracts').Joi

module.exports = new Contract({
  name: 'Users',
  consumer: 'Jsonplaceholder API',
  request: {
    method: 'GET',
    url: 'https://jsonplaceholder.typicode.com/users'
  },
  response: {
    statusCode: 200,
    body: Joi.array().items(
      Joi.object().keys({
        id: Joi.number(),
        name: Joi.string(),
        username: Joi.string(),
        email: Joi.string(),
        address: Joi.object()
      })
    )
  }
})
