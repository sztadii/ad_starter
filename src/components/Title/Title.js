import React from 'react'
import PropTypes from 'prop-types'

const Title = ({ children }) => <div className='Title'>{children}</div>

Title.propTypes = {
  children: PropTypes.node.isRequired
}

export default Title
