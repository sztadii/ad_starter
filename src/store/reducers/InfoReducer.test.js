import { InfoGetLoadingAction, InfoGetErrorAction } from '../actions/InfoAction'
import { InfoReducer } from './InfoReducer'

describe('InfoReducer', () => {
  it('returns default state after init', () => {
    const expectState = {
      loading: false,
      list: [],
      error: ''
    }
    expect(InfoReducer(undefined, {})).toEqual(expectState)
  })

  it('returns loading state after trigger InfoGetLoadingAction', () => {
    const expectState = {
      loading: true,
      list: [],
      error: ''
    }
    expect(InfoReducer(undefined, InfoGetLoadingAction())).toEqual(expectState)
  })

  it('returns error state after trigger InfoGetErrorAction', () => {
    const expectState = {
      loading: false,
      list: [],
      error: 'Error message'
    }
    expect(InfoReducer(undefined, InfoGetErrorAction('Error message'))).toEqual(
      expectState
    )
  })
})
