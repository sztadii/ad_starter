import { HomeGetLoadingAction, HomeGetErrorAction } from '../actions/HomeAction'
import { HomeReducer } from './HomeReducer'

describe('HomeReducer', () => {
  it('returns default state after init', () => {
    const expectState = {
      loading: false,
      list: [],
      error: ''
    }
    expect(HomeReducer(undefined, {})).toEqual(expectState)
  })

  it('returns loading state after trigger HomeGetLoadingAction', () => {
    const expectState = {
      loading: true,
      list: [],
      error: ''
    }
    expect(HomeReducer(undefined, HomeGetLoadingAction())).toEqual(expectState)
  })

  it('returns error state after trigger HomeGetErrorAction', () => {
    const expectState = {
      loading: false,
      list: [],
      error: 'Error message'
    }
    expect(HomeReducer(undefined, HomeGetErrorAction('Error message'))).toEqual(
      expectState
    )
  })
})
