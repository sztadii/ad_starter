import {
  HOME_GET_LOADING,
  HOME_GET_SUCCESS,
  HOME_GET_ERROR
} from '../actions/HomeAction'
import { initState } from '../main/initState'

const init = initState.HomeReducer

export function HomeReducer (state = init, action) {
  switch (action.type) {
    case HOME_GET_LOADING: {
      return {
        ...state,
        loading: true,
        error: ''
      }
    }

    case HOME_GET_SUCCESS: {
      return {
        ...state,
        list: action.list,
        loading: false
      }
    }

    case HOME_GET_ERROR: {
      return {
        ...state,
        loading: false,
        error: action.error
      }
    }
  }

  return state
}
