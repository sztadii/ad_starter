import * as selectors from './selectors'
import { initState } from './initState'

describe('homeLoading', () => {
  it('returns value of home loading', () => {
    expect(selectors.homeLoading(initState)).toEqual(false)
  })
})

describe('homeList', () => {
  it('returns value of home list', () => {
    expect(selectors.homeList(initState)).toEqual([])
  })
})

describe('homeError', () => {
  it('returns value of home error', () => {
    expect(selectors.homeError(initState)).toEqual('')
  })
})

describe('infoLoading', () => {
  it('returns value of info loading', () => {
    expect(selectors.infoLoading(initState)).toEqual(false)
  })
})

describe('infoList', () => {
  it('returns value of info list', () => {
    expect(selectors.infoList(initState)).toEqual([])
  })
})

describe('infoError', () => {
  it('returns value of info error', () => {
    expect(selectors.infoError(initState)).toEqual('')
  })
})
