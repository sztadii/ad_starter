export const homeLoading = state => state.HomeReducer.loading
export const homeList = state => state.HomeReducer.list
export const homeError = state => state.HomeReducer.error

export const infoLoading = state => state.InfoReducer.loading
export const infoList = state => state.InfoReducer.list
export const infoError = state => state.InfoReducer.error
