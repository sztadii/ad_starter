import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import reducer from '../reducers/index'
import saga from '../middleware/index'

const sagaMiddleware = createSagaMiddleware()
const store = getStore()

function getStore () {
  if (process.env.NODE_ENV === 'production' || !process.env.DEBUG) {
    return createStore(reducer, applyMiddleware(sagaMiddleware))
  } else {
    const { createLogger } = require('redux-logger')
    const logger = createLogger({
      collapsed: true
    })
    return createStore(reducer, applyMiddleware(logger, sagaMiddleware))
  }
}

sagaMiddleware.run(saga)

export default store
