export const INFO_GET = 'INFO_GET'
export const InfoGetAction = () => ({
  type: INFO_GET
})

export const INFO_GET_LOADING = 'INFO_GET_LOADING'
export const InfoGetLoadingAction = () => ({
  type: INFO_GET_LOADING
})

export const INFO_GET_SUCCESS = 'INFO_GET_SUCCESS'
export const InfoGetSuccessAction = list => ({
  type: INFO_GET_SUCCESS,
  list
})

export const INFO_GET_ERROR = 'INFO_GET_ERROR'
export const InfoGetErrorAction = error => ({
  type: INFO_GET_ERROR,
  error
})
