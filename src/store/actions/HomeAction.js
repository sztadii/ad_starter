export const HOME_GET = 'HOME_GET'
export const HomeGetAction = (refresh = false) => ({
  type: HOME_GET,
  refresh
})

export const HOME_GET_LOADING = 'HOME_GET_LOADING'
export const HomeGetLoadingAction = () => ({
  type: HOME_GET_LOADING
})

export const HOME_GET_SUCCESS = 'HOME_GET_SUCCESS'
export const HomeGetSuccessAction = list => ({
  type: HOME_GET_SUCCESS,
  list
})

export const HOME_GET_ERROR = 'HOME_GET_ERROR'
export const HomeGetErrorAction = error => ({
  type: HOME_GET_ERROR,
  error
})
