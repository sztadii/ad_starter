import { takeLatest } from 'redux-saga/effects'
import * as actions from '../../actions/InfoAction'
import * as workers from '../workers/workerInfo'

export function * watchInfo () {
  yield takeLatest(actions.INFO_GET, workers.getInfo)
}
