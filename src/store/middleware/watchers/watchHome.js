import { takeLatest } from 'redux-saga/effects'
import * as actions from '../../actions/HomeAction'
import * as workers from '../workers/workerHome'

export function * watchHome () {
  yield takeLatest(actions.HOME_GET, workers.getHome)
}
