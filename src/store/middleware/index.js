import { fork } from 'redux-saga/effects'
import { watchHome } from './watchers/watchHome'
import { watchInfo } from './watchers/watchInfo'

export default function * () {
  yield [fork(watchHome), fork(watchInfo)]
}
