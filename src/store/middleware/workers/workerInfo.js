import { put, call } from 'redux-saga/effects'
import {
  InfoGetLoadingAction,
  InfoGetSuccessAction,
  InfoGetErrorAction
} from '../../actions/InfoAction'
import api from '../../../api/api'

export function * getInfo () {
  yield put(InfoGetLoadingAction())

  try {
    const { data: info } = yield call(api.get, api.routes.users)
    yield put(InfoGetSuccessAction(info))
  } catch (e) {
    yield put(InfoGetErrorAction(e.message))
  }
}
