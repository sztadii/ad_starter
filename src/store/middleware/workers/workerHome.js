import { delay } from 'redux-saga'
import { put, call } from 'redux-saga/effects'
import {
  HomeGetLoadingAction,
  HomeGetSuccessAction,
  HomeGetErrorAction
} from '../../actions/HomeAction'
import api from '../../../api/api'

export function * getHome (action) {
  yield put(HomeGetLoadingAction())

  if (action.refresh) yield delay(500)

  try {
    const { data: home } = yield call(api.get, api.routes.users)
    yield put(HomeGetSuccessAction(home))
  } catch (e) {
    yield put(HomeGetErrorAction(e.message))
  }
}
