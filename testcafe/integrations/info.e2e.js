import InfoPage from '../pages/InfoPage'
import routes from '../../src/config/routes'

fixture`Info page`.page`${routes.baseUrl}${routes.info}`

const info = new InfoPage()

test('Info render properly', async t => {
  await info.checkTable(t)
})
