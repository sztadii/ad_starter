import HomePage from '../pages/HomePage'
import routes from '../../src/config/routes'

fixture`Home page`.page`${routes.baseUrl}${routes.home}`

const home = new HomePage()

test('Home render properly', async t => {
  await home.checkTable(t)
})
