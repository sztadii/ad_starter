import { Selector } from 'testcafe'

export default class HomePage {
  constructor () {
    this.tableRow = Selector('tr')
  }

  async checkTable (t) {
    await t
      .click(this.tableRow)
      .expect(this.tableRow.count)
      .gt(1)
  }
}
