## CircleCI status
[![CircleCI](https://circleci.com/bb/sztadii/ad_starter.svg?style=svg)](https://circleci.com/bb/sztadii/ad_starter)

## This project use
[![js-standard-style](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

## Tested with TestCafe
[![TestCafe](https://img.shields.io/badge/tested%20with-TestCafe-2fa4cf.svg)](https://github.com/DevExpress/testcafe)

## How run project
* Install [NodeJs](https://nodejs.org/en/download)
* Install all dependencies via npm
* Serve or build application with using commands `npm run serve-dev` or `npm run start` 

## All important commands (commands with prefix '_' are depends by other, so you need to run other commands before)
* `npm i` to install dependencies
* `npm start` to start developing app in best way
* `npm run serve-dev` to serve app with webpack-serve
* `npm run build-prod` to build app in production
* `npm run _serve-prod` to run app on production server
* `npm run analyzer` to run bundle analytics tool
* `npm run storybook` to run storybook tool
* `npm run test` to run unit and e2e tests
* `npm run test-bundle-size` to test bundle size on production
* `npm run unit` to run unit tests
* `npm run unit-watch` to run tests with watch mode (good when developing)
* `npm run unit-update` to update snapshots tests
* `npm run e2e-start` to run e2e tests with webpack dev server (we check webpack still works correctly)
* `npm run _e2e-prod` to run e2e tests with production server
* `npm run _e2e` to run e2e tests
* `sudo npm cache clean -f & sudo npm install -g n & sudo n stable` to update your node to latest stable version

## Short desc and links to libraries
* [React](https://reactjs.org) - view library
* [Redux](https://redux.js.org) - state management
* [Redux-Saga](https://redux-saga.js.org) - Redux middleware Using saga to make test life cycle easier with help of generators
* [React-Router](https://reacttraining.com/react-router/web) - application routing
* [Webpack](https://webpack.js.org/concepts) - build tool
* [Babel](https://babeljs.io) - compiler ES6 & ES7 codes to ES5
* [SCSS](http://sass-lang.com/documentation) - to make use of programing over css
* [ESLint](http://eslint.org/docs/rules) - standard roles for js
* [ESLint React](https://github.com/yannickcr/eslint-plugin-react) - standard roles for react
* [StyleLint](https://stylelint.io/user-guide/rules)
* [Editorconfig](http://editorconfig.org) 
* [Lodash](https://lodash.com/docs/4.17.4) - for many helpers functions
* [Jest](http://facebook.github.io/jest) - runner for unit tests
* [Testcafe](https://devexpress.github.io/testcafe/documentation/test-api/a-z.html) - for e2e tests
* [ConcumerContracts](https://github.com/bbc/consumer-contracts) - for api tests
* [Storybook](https://storybook.js.org/basics/introduction/) - for documentation and presentation components
    
## Our conventions
* [BEM](http://getbem.com) - methodology for reusable elements